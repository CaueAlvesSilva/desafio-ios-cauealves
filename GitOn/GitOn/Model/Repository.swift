//
//  Repository.swift
//  GitOn
//
//  Created by Cauê Silva on 18/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repository: Mappable {
    
    var name = ""
    var description = ""
    var stars = 0
    var forks = 0
    private var owner: Owner?
    var ownerName: String {
        return owner?.name ?? ""
    }
    var ownerImage: String {
        return owner?.image ?? ""
    }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
    }
    
    mutating func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        stars <- map["watchers"]
        forks <- map["forks"]
        owner <- map["owner"]
    }
}
