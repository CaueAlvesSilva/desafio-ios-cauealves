//
//  Languages.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import UIKit

enum Languages: String {
    case swift = "Swift"
    case objectiveC = "ObjectiveC"
    case javaScript = "JavaScript"
    case java = "Java"
    case kotlin = "Kotlin"
    case python = "Python"
    case ruby = "Ruby"
    case php = "PHP"
    case c = "C"
    case go = "Go"
    
    static let allValues = [swift, objectiveC, javaScript, java, kotlin, python, ruby, php, c, go]
    
    var icon: UIImage {
        return UIImage(named: "icon_\(rawValue.lowercased())")!
    }
    
}
