//
//  PullRequest.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import ObjectMapper

struct PullRequest: Mappable {
    
    var title = ""
    var descripton = ""
    var date = ""
    private var owner: Owner?
    var authorName: String {
        return owner?.name ?? ""
    }
    var authorImage: String {
        return owner?.image ?? ""
    }
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    init() {
    }
    
    mutating func mapping(map: Map) {
        title <- map["title"]
        descripton <- map["body"]
        date <- map["created_at"]
        owner <- map["user"]
    }
}
