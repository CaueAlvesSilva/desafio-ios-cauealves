//
//  Transporter.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation

class Transporter<T> {
    
    var data: T!
    
    init(data: T) {
        self.data = data
    }
    
    init() {
    }
    
}
