//
//  RepositoriesTableViewCell.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import UIKit

import PINCache
import PINRemoteImage

class RepositoriesTableViewCell: UITableViewCell {

    let view: RepositoriesView = UIView.fromNib()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        fillWithSubview(subview: view)
    }
    
    func fill(repo: Repository) {
        view.fill(repo: repo)
    }
    
}
