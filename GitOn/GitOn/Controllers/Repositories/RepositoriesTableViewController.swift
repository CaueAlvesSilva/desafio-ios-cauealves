//
//  RepositoriesTableViewController.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import UIKit

class RepositoriesTableViewController: UITableViewController, RepositoriesDelegate {

    private lazy var viewModel: RepositoriesViewModel = RepositoriesViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = viewModel.title
        fetchRepositories()
    }
    
    func prepareForNavigation(transporter: Transporter<Any>) {
        viewModel.prepareForNavigation(transporter: transporter)
    }
    
    private func fetchRepositories() {
        // startLoader
        viewModel.fetchRepositories()
    }

    // MARK: UITableViewDelegate & UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRepos
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RepositoriesTableViewCell = UITableViewCell.createCell(tableView: tableView, indexPath: indexPath)
        if let repo = viewModel.repo(at: indexPath.row) {
            cell.fill(repo: repo)
        }
        return cell
    }
    
    // MARK: RepositoriesDelegate
    
    func fetchedRepositories(with success: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            if !success {
                // connection error alert
            }
        }
    }
    
}
