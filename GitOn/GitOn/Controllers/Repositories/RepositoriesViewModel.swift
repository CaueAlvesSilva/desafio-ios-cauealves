//
//  RepositoriesViewModel.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation

protocol RepositoriesDelegate: class {
    func fetchedRepositories(with success: Bool)
}

class RepositoriesViewModel {
    
    private var remoteService: RepositoriesService?
    private weak var delegate: RepositoriesDelegate?
    
    private var repos = [Repository]()
    private var language: Languages?
    private var page = 0
    
    init(delegate: RepositoriesDelegate?, serviceType: RepositoriesService.Type = RepositoriesRequest.self) {
        self.delegate = delegate
        remoteService = serviceType.init()
    }
    
    func prepareForNavigation(transporter: Transporter<Any>) {
        if let language = transporter.data as? Languages {
            self.language = language
        }
    }
    
    var title: String {
        return language?.rawValue ?? ""
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    var numberOfRepos: Int {
        return repos.count
    }
    
    func fetchRepositories() {
        guard let service = remoteService, let language = language else {
            return
        }
        service.fetchRepositories(for: language, page: page, completion: { repos, error in
            guard let repos = repos, error == nil else {
                self.delegate?.fetchedRepositories(with: false)
                return
            }
            self.repos = repos
            self.delegate?.fetchedRepositories(with: true)
        })
    }
    
    func repo(at index: Int) -> Repository? {
        return repos.object(index: index)
    }
    
}
