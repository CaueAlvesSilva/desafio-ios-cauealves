//
//  RepositoriesView.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import UIKit

class RepositoriesView: UIView {
    
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var ownerImageView: UIImageView!
    @IBOutlet weak var ownerNameLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(repo: Repository) {
        repoNameLabel.text = repo.name
        ownerImageView.downloadImage(with: repo.ownerImage)
        ownerNameLabel.text = repo.ownerName
        starsLabel.text = "\(repo.stars)"
        forksLabel.text = "\(repo.forks)"
    }
    
}
