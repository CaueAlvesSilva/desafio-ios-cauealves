//
//  LanguagesViewModel.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation

class LanguagesViewModel {
    
    private var languages = Languages.allValues
    
    init() {
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    var numberOfLanguages: Int {
        return languages.count
    }
    
    func language(for index: Int) -> Languages {
        return languages.object(index: index) ?? .swift
    }
    
}
