//
//  LanguagesCollectionViewCell.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import UIKit

class LanguagesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var languageImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(language: Languages) {
        languageLabel.text = language.rawValue
        languageImageView.image = language.icon
    }

}
