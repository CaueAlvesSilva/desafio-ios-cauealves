//
//  LanguagesCollectionViewController.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import UIKit

class LanguagesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    private lazy var viewModel = LanguagesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeNavigation()
        collectionView?.registerCell(identifier: String(describing: LanguagesCollectionViewCell.self))
    }

    // MARK: UICollectionViewDelegate & UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return viewModel.numberOfSections
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfLanguages
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 200)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: LanguagesCollectionViewCell = UICollectionViewCell.createCell(collectionView: collectionView, indexPath: indexPath)
        cell.fill(language: viewModel.language(for: indexPath.item))
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "reposSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let reposController = segue.destination as? RepositoriesTableViewController, let selectedIndex = collectionView?.indexPathsForSelectedItems?.first?.item {
            reposController.prepareForNavigation(transporter: Transporter(data: viewModel.language(for: selectedIndex)))
        }
    }
    
}
