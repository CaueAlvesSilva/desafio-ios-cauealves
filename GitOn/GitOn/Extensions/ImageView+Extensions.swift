//
//  ImageView+Extensions.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import UIKit
import PINCache
import PINRemoteImage

extension UIImageView {
    
    func downloadImage(with url: String) {
        pin_updateWithProgress = true
        image = nil
        pin_setImage(from: URL(string: url))
    }
    
}
