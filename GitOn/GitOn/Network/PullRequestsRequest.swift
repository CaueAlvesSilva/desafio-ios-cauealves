//
//  PullRequestsRequest.swift
//  GitOn
//
//  Created by Cauê Silva on 19/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import ObjectMapper

protocol PullRequestsService {
    init()
    func fetchPullRequests(for owner: String, repo: String, completion: @escaping (_ pulls: [PullRequest]?, _ error: ErrorType?) -> Void)
}

class PullRequestsRequest: PullRequestsService {
    
    required init() {
    }
    
    func fetchPullRequests(for owner: String, repo: String, completion: @escaping (_ pulls: [PullRequest]?, _ error: ErrorType?) -> Void) {
        Network(action: .pullRequests(owner, repo), method: .get).makeRequest { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            let result = response.result as? [JSONDictionary] <*> (PullRequest.self, response.error)
            completion(result.object, result.error)
        }
    }
    
}

class MockPullRequestsRequest: PullRequestsService {
    
    required init() {
    }
    
    func fetchPullRequests(for owner: String, repo: String, completion: @escaping (_ pulls: [PullRequest]?, _ error: ErrorType?) -> Void) {
        let path = Bundle.main.path(forResource: "MockPullRequests", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! [JSONDictionary]
        let result = jsonResult.flatMap { PullRequest(map: Map(mappingType: .fromJSON, JSON: $0)) }
        completion(result, nil)
    }
    
}
