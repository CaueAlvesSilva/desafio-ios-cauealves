//
//  RepositoriesRequest.swift
//  GitOn
//
//  Created by Cauê Silva on 18/09/17.
//  Copyright © 2017 Caue Alves. All rights reserved.
//

import Foundation
import ObjectMapper

protocol RepositoriesService {
    init()
    func fetchRepositories(for language: Languages, page: Int, completion: @escaping (_ repos: [Repository]?, _ error: ErrorType?) -> Void)
}

class RepositoriesRequest: RepositoriesService {
    
    required init() {
    }
    
    func fetchRepositories(for language: Languages, page: Int, completion: @escaping (_ repos: [Repository]?, _ error: ErrorType?) -> Void) {
        Network(action: .repositories(language.rawValue, "\(page)"), method: .get).makeRequest { response in
            if let error = response.error {
                completion(nil, error)
                return
            }
            let searchResult = response.result <*> (Search.self, response.error)
            completion(searchResult.object?.repos, searchResult.error)
        }
    }
    
}

class MockRepositoriesRequest: RepositoriesService {
    
    required init() {
    }
    
    func fetchRepositories(for language: Languages, page: Int, completion: @escaping (_ repos: [Repository]?, _ error: ErrorType?) -> Void) {
        let path = Bundle.main.path(forResource: "MockRepositories", ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!))
        let jsonResult = try! JSONSerialization.jsonObject(with: data, options: []) as! JSONDictionary
        let searchResult = (jsonResult <*> (Search.self, nil))
        completion(searchResult.object?.repos, nil)
    }
    
}
